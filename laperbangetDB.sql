/*
Navicat MySQL Data Transfer

Source Server         : Angga
Source Server Version : 50525
Source Host           : localhost:3306
Source Database       : laperbangetdb

Target Server Type    : MYSQL
Target Server Version : 50525
File Encoding         : 65001

Date: 2013-03-26 12:57:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('5d74cd045a08ed69b7452a819e13abfa', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22', '1364265961', 'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:5:{s:8:\"id_admin\";s:1:\"1\";s:8:\"username\";s:10:\"locklems90\";s:5:\"level\";s:3:\"top\";s:5:\"email\";s:20:\"masangga@outlook.com\";s:13:\"last_login_ip\";s:3:\"::1\";}}');
INSERT INTO `ci_sessions` VALUES ('dc23af3156d29c4c36463dadb443f7c1', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22', '1364276919', 'a:2:{s:9:\"user_data\";s:0:\"\";s:5:\"admin\";a:5:{s:8:\"id_admin\";s:1:\"1\";s:8:\"username\";s:10:\"locklems90\";s:5:\"level\";s:3:\"top\";s:5:\"email\";s:20:\"masangga@outlook.com\";s:13:\"last_login_ip\";s:3:\"::1\";}}');

-- ----------------------------
-- Table structure for `tbl_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nama_lengkap` varchar(45) DEFAULT NULL,
  `level` varchar(10) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `last_login_ip` varchar(20) DEFAULT NULL,
  `waktu_buat` date DEFAULT '0000-00-00',
  `status` varchar(15) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_admin
-- ----------------------------
INSERT INTO `tbl_admin` VALUES ('1', 'locklems90', '81dc9bdb52d04dc20036dbd8313ed055', 'septioadi anggara ', 'top', 'masangga@outlook.com', '::1', '2013-02-18', 'pending');
INSERT INTO `tbl_admin` VALUES ('3', 'popok', '81dc9bdb52d04dc20036dbd8313ed055', 'popo', 'top', 'masangga@outlook.com', null, '2013-03-28', 'active');

-- ----------------------------
-- Table structure for `tbl_ads`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ads`;
CREATE TABLE `tbl_ads` (
  `idads` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`idads`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_ads
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_kurir`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kurir`;
CREATE TABLE `tbl_kurir` (
  `id_kurir` int(11) NOT NULL DEFAULT '0',
  `nama` varchar(100) DEFAULT NULL,
  `no_kontak` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_ktp` varchar(20) DEFAULT NULL,
  `status_motor` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_kurir`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_kurir
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(45) NOT NULL,
  `harga` double NOT NULL,
  `deskripsi` text,
  `picture` varchar(100) DEFAULT NULL,
  `id_restaurant` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`),
  KEY `fk_menumakanan_restaurant1_idx` (`id_restaurant`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_menumakanan_has_order`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menumakanan_has_order`;
CREATE TABLE `tbl_menumakanan_has_order` (
  `menumakanan_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`menumakanan_id`,`order_id`),
  KEY `fk_menumakanan_has_order_order1_idx` (`order_id`),
  KEY `fk_menumakanan_has_order_menumakanan_idx` (`menumakanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_menumakanan_has_order
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_order`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
  `id_order` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `totalprice` float NOT NULL,
  `status` enum('terkirim','bermasalah') DEFAULT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_order
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_promo`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_promo`;
CREATE TABLE `tbl_promo` (
  `idpromo` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `deskripsi` varchar(45) DEFAULT NULL,
  `picture` varchar(45) NOT NULL,
  PRIMARY KEY (`idpromo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_promo
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_restaurant`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_restaurant`;
CREATE TABLE `tbl_restaurant` (
  `id_restaurant` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `deskripsi` varchar(45) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `fb_fanpage` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_restaurant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_restaurant
-- ----------------------------
INSERT INTO `tbl_restaurant` VALUES ('0', 'Shafa Yogurt', 'lalalala', 'jalan lalala', '1234', 'panggil.aku.angga@gmail.com', null, null);

-- ----------------------------
-- Table structure for `tbl_subscriber`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_subscriber`;
CREATE TABLE `tbl_subscriber` (
  `idsubscriber` int(11) NOT NULL,
  `email_phone` varchar(45) NOT NULL,
  PRIMARY KEY (`idsubscriber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_subscriber
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_teaser`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_teaser`;
CREATE TABLE `tbl_teaser` (
  `id_teaser` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(70) NOT NULL,
  `time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(30) NOT NULL,
  `twitter_name` varchar(255) DEFAULT NULL,
  `twitter_friends` int(11) DEFAULT NULL,
  `twitter_username` varchar(40) DEFAULT NULL,
  `facebook_id` varchar(50) DEFAULT NULL,
  `fb_name` varchar(200) DEFAULT NULL,
  `fb_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_teaser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_teaser
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `alamat` varchar(45) NOT NULL,
  `password` varchar(224) NOT NULL,
  `email` varchar(45) NOT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `twitter_id` varchar(100) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `point` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
