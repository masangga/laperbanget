<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=IE7">
    
    <meta name="keywords" content="food delivery bandung, jatinangor, order food online, delivery service, bandung, jatinangor, cileunyi, cinunuk, food, restaurant delivery, laperbanget, laparbanget,lapar,banget, hungry, pesan makan, pesan, makan" />
    <meta name="robots" content="noodp,noydir" />
    <meta name="allow-search" content="yes" />
    <meta name="audience" content="all" />
    <meta name="distribution" content="global" />
    <meta name="document-classification" content="general" />
    <meta name="rating" content="general" />
     <meta property="fb:admins" content="1394565663"/>
    <!-- <meta property="fb:page_id" content="216747471668883"/> -->
    <meta property="og:image" content="http://www.laperbanget.com/assets/teaserasset/img/transparent_icon_200.png"/>
    <meta property="og:title" content="Laperbanget.com | Online Food Delivery System" />
    <meta property="og:description" content="laperbanget? kamu pesen, kami anter. Online Food Order Delivery in Jatinangor. " />
	
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?php echo $title ?>" />
    <meta property="og:url" content="http://www.laperbanget.com/teaser/subscribe/"/>



    <meta property="description" content="laperbanget? kamu pesen, kami anter. Online Food Order Delivery in Jatinangor. " />
    <meta name="description" content="laperbanget? kamu pesen, kami anter. Online Food Order Delivery in Jatinangor. " />
    <meta property="name" content="laperbanget? kamu pesen, kami anter. Online Food Order Delivery in Jatinangor." />
    <title><?php echo $title ?></title>
        <link rel="icon"  type="image/png" href="<?php echo local_path('assets/teaserasset/img/logo.png'); ?>">
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/tooltip.css'); ?> " type="text/css" media="screen"> 
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/reset.css'); ?>" type="text/css" media="screen"> 
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/style.css'); ?> " type="text/css" media="screen"> 
     <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/faq.css'); ?> " type="text/css" media="screen"> 
    <!-- Add jQuery library -->
    <script type="text/javascript" src="<?php echo local_path('assets/js/jquery-1.8.2.min.js'); ?>"></script>
    <style type="text/css">
        .bottom-fixed{
          position: fixed;
          bottom: 0px;
          width: 100%;
        }
    </style>
    <link href="http://fonts.googleapis.com/css?family=Sofia" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="/(path)/tooltip.css" />
    <script type="text/javascript" src="<?php echo local_path('assets/js/tooltip.js'); ?>"></script>

  </head>

  <body class="wrappersubscribe">
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=144803309006640";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
   
      <div class="container" id="subscribe" >
            <div class="wrapper">
              <div id="subscribe-title">
                  <h1 class="left">Daftar</h1>
                  <div class="border"></div> 
                  <div id="logo">
                    <img src="<?php echo local_path('assets/teaserasset/img/logo.png'); ?>" alt="laperbanget" />
                  </div>
                 
                </div> 
          
                <?php echo form_open('home/doDaftar'); ?>
              <div id="leftside">
                <h5>Daftar sekarang juga untuk memesan di laperbanget.com</h5>
                <div class="box">
                  <div class="reg-column">
                      <h5>Nama
                        <span class="tooltip" onmouseover="tooltip.pop(this,'<span class=\'sesuatu\'>Hi</span> there',{position:0})">?</span> </h5> 
                      <input type="text" placeholder="nama" id="nama" name="nama" value="<?php  
                      if ($this->session->flashdata('nama')) echo $this->session->flashdata('nama'); ?>"/> 
                  </div>
                  <div class="reg-column">
                      <h5>Email<span class="tooltip" onmouseover="tooltip.pop(this,'<span class=\'red\'>Hi</span> there1',{position:0})">?</span></h5>
                      <input type="text" placeholder="email" id="email" name="email" value="<?php  
                      if ($this->session->flashdata('email')) echo $this->session->flashdata('email'); ?>"/> 
                  </div>
                  <div class="reg-column">
                      <h5>Handphone<span class="tooltip" onmouseover="tooltip.pop(this,'<span class=\'red\'>Hi</span> there21',{position:0})">?</span></h5>
                      <input type="text" placeholder="handphone" id="handphone" name="handphone"/> 
                  </div>                  
                  <div class="reg-column">
                      <h5>Alamat<span class="tooltip" onmouseover="tooltip.pop(this,'<span class=\'red\'>Hi</span> there13',{position:0})">?</span></h5>
                      <input type="text" placeholder="alamat" id="alamat" name="alamat"/> 
                  </div>
                  <div class="reg-column">
                      <h5>Password<span class="tooltip" onmouseover="tooltip.pop(this,'<span class=\'red\'>Hi</span> there41',{position:0})">?</span></h5>
                      <input type="password" placeholder="password" id="password" name="password"/> 
                  </div>
                  <div id="term-and-policy">
                    <input type="checkbox" name="terms" />
                    <span>I have agreed <a href="#">Terms of Use Agreement</a> and <a href="#">Privacy Policy</a></span>
                    <input type="submit" class="submit-btn" id="submit-btn"/>
                  </div>
                </div>
                <?php echo form_close(); ?>
                
              </div>
              <div id="rightside">
                <?php //$attributes= array('id' => 'myForm'); echo form_open('teaser/addSubscriber',$attributes);?>
                
                <?php //echo form_close(); ?>
               
                <div class="box">
                  <h4>Atau daftar melalui akun-mu di :</h4>
                  <div id="image-box">
                    <a href="<?php echo local_path('home/facebook_daftar') ?>">
                    <img src="<?php echo local_path('assets/teaserasset/img/facebook.png'); ?>" alt=""/>
                  </a>
                  <a href="<?php echo local_path('home/signup_with_twitter') ?>">
                    <img src="<?php echo local_path('assets/teaserasset/img/twitter.png'); ?>" alt="" />
                  </a>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <footer class="bottom-fixed">
           
            <div class="wrapper">
                <div class="footer-signin left">
                  Sudah punya akun laperbanget ? <a href="<?php echo local_path('home/login'); ?>">Masuk disini</a> 
                </div>
				
                 
                
            </div>
			
        </footer>

<script>!function(d,s,id){
    var js,fjs=d.getElementsByTagName(s)[0];
    if(!d.getElementById(id)){
        js=d.createElement(s);js.id=id;
        js.src="//platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js,fjs);}}
        (document,"script","twitter-wjs");
        </script>

        <script type="text/javascript">
        var submit =  $('#submit-btn');
        var email = $('#email');
        var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
        $(document).ready(function() {
          submit.click(function()  {
            if (filter.test(email.val()))
            {
              
              $.ajax({
                url: "<?php echo local_path("teaser/addSubscriber")?>",
                data: 'email='+email.val(),
                dataType: 'text',
                type: 'POST',
                success: function (result)
                {
                  if (result == 'adding')
                  {
                    alert("terima kasih sudah mensubscribe!!");
                  }
                  else if(result == 'exist')
                  {
                    alert("email sudah terdaftar");
                  }
                }
              });

              //alert(email.val());  
            }
            else
            {
              alert('Mohon masukkan format email dengan benar!');
            }
            
          });
        });
        </script>
        <?php $this->load->view('google_analytics'); ?>
  </BODY>

</HTML>