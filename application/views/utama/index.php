<!DOCTYPE html>
<HTML lang="en">
<HEAD>
  <title><?php echo $title; ?></title>
  <link rel="icon"  type="image/png" href="asset/img/logo.png">
  <link rel="stylesheet" href="<?php echo local_path('assets/utama/css/reset.css') ?>" type="text/css" media="screen"> 
  <link rel="stylesheet" href="<?php echo local_path('assets/utama/css/style.css') ?>" type="text/css" media="screen"> 
  <link href='http://fonts.googleapis.com/css?family=Sofia' rel='stylesheet' type='text/css'>
</HEAD>
<BODY>
  <?php $this->load->view('utama/header'); ?>
  <div id="container">
    <div class="wrapper">
      <div class="left">
          <div id="user">
            <div id="photoUser" class="left"><img src=""></div>
            <div id="detailUser" class="left">
              <div id="username"><a href="#">Loreim ipsum dolor</a></div>
              <div id="kosan">Wisma putri kamar 2A</div>
              <div id="alamat">Jl. Sukabirus no. 42</div>
            </div>
          </div>
      </div>
      <div class="right">
          <div id="cart_section">
            <div id="cart_button">Checkout <span id="total_items">X</span></div>
          </div>
      </div>    
      
      
      <!-------------- KATEGORI RESTORAN ----------------------------->
      <div id="subtitle">
        <ul>
          <li id="cafe">Cafe</li>
          <li id="rmpadang">RM Padang</li>
          <li id="minuman">Minuman</li>
          <li id="jajanan">Jajanan</li>
        </ul>
      </div>
      <!-- SUBTITLE END -->

      <div id="content">
        <div class="left">
            <div class="content-section">
                <div class="circle left">
                </div>
                <hr class="border"/>
                <div class=" detail-content left">            
                    <div class="gambar_menu">
                      <img src="" alt="" />
                    </div>
                    <div class="nama_partner">
                      <h5><a href="#">Lorem Ipsum Dolor</a></h5>
                    </div>
                    <div class="status buka">
                      Buka
                    </div>
                    
                    <div class="alamat_partner"> Lorem ipsum dolor sit amet</div>
                    <ul class="food-list">
                        <li class="first" ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                    </ul>
                </div>
                
            </div>
            
            <div class="content-section">
                <div class="circle left">
                </div>
                <hr class="border"/>
                <div class=" detail-content left">            
                    <div class="gambar_menu">
                      <img src="" alt="" />
                    </div>
                    <div class="nama_partner">
                      <h5><a href="#">Lorem Ipsum Dolor</a></h5>
                    </div>
                    <div class="status tutup">
                      Tutup
                    </div>
                    
                    <div class="alamat_partner"> Lorem ipsum dolor sit amet</div>
                    <ul class="food-list">
                        <li class="first" ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                        <li ><img src="" alt="" /></li>
                    </ul>
                </div>
                
            </div>
            
           
        </div>
        <div class="right">
          <div class="ads"><p>advertising</p></div>
        </div>
      </div>

      </div>
    </div>
</BODY>
</HTML>