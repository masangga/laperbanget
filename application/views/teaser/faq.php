<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=IE7">
    
    <meta name="keywords" content="food delivery bandung, jatinangor, order food online, delivery service, bandung, jatinangor, cileunyi, cinunuk, food, restaurant delivery, laperbanget, laparbanget,lapar,banget, hungry, pesan makan, pesan, makan" />
    <meta name="robots" content="noodp,noydir" />
    <meta name="allow-search" content="yes" />
    <meta name="audience" content="all" />
    <meta name="distribution" content="global" />
    <meta name="document-classification" content="general" />
    <meta name="rating" content="general" />
   
    <meta property="description" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor. " />
    <meta name="description" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor. " />
    <meta property="name" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor." />

     <meta property="fb:admins" content="1394565663"/>
    <!-- <meta property="fb:page_id" content="216747471668883"/> -->
    <meta property="og:image" content="http://www.laperbanget.com/assets/teaserasset/img/transparent_icon_200.png"/>
    <meta property="og:title" content="Laperbanget.com | Online Food Delivery System" />
    <meta property="og:description" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor. " />
	
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?php echo $title ?>" />
    <meta property="og:url" content="http://www.laperbanget.com/faq/"/>

    <title><?php echo $title ?></title>
        <link rel="icon"  type="image/png" href="<?php echo local_path('assets/teaserasset/img/logo.png'); ?>">
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/reset.css'); ?>" type="text/css" media="screen"> 
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/style.css'); ?> " type="text/css" media="screen"> 
     <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/faq.css'); ?> " type="text/css" media="screen"> 
    <!-- Add jQuery library -->
    <script type="text/javascript" src="<?php echo local_path('assets/js/jquery-1.8.2.min.js'); ?>"></script>

   
  </head>

  <body>
   <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=144803309006640";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <header>
      <div class="wrapper">
        <div id="logo">
          <img src="<?php echo local_path('assets/teaserasset/img/logo.png'); ?>" alt="" />
        </div>
        <div id="top-nav"> 
          <ul id="nav-menu">
<li class="active"> <a href="<?php echo local_path('teaser/faq'); ?>"><span class="yellow-bold">Apakah itu </span> laperbanget ? </a></li>
            <!--<li> <a href="#"><span class="yellow-bold">Lihat katalog </span>menu</a> </li>-->
            <li> <a href="<?php echo local_path('teaser/faq'); ?>"><span class="yellow-bold">Bagaimana </span>jika ingin menjadi Mitra ? </a></li>
			<li> <a href="<?php echo local_path('teaser/subscribe'); ?>"><span class="yellow-bold">Segera </span>Subscribe!! </a></li>

          </ul>
          <div class="fb-btn right">
              <div class="fb-like" data-href="http://laperbanget.com" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="segoe ui" data-action="recommend"></div>


          </div>   
          <div class="twitter-btn right">
             <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://laperbanget.com" data-text="Jatinangor sekarang bisa delivery dimana saja" data-via="laperbanget" data-hashtags="laperbanget"
             >Tweet</a>
          </div>    
    </div>
    </header>

       <div class="container">
      <div class="wrapper">
            <h1 >FAQ</h1> 
			<div class="title">
                <span class="stabilo">Tentang Kami</span>
            </div>
            <div class="content">
                Laperbanget.com hadir khusus buat kamu mahasiswa yang lagi laperbanget atau laper aja atau cuma pengen ngemil doang, karena kami adalah website yang melayani pemesanan makanan secara online. Di laperbanget.com kamu bisa memilih menu yang bisa dipesan dari beberapa  restoran, warung makan, sampai jajanan kaki lima di sekitar kamu kapanpun dan dimanapun kamu berada di Daerah Jatinangor dan sekitanya. Langkah2nya  adalah Lihat menu, harga, foto dan langsung pesan online langsung melalui situs resmi laperbanget.com ataupun melewati sms, Gampang banget kan..
            </div>
			<div class="title">
                <span class="stabilo">Apakah saya harus mendaftar sebagai member untuk melakukan pemesanan secara online?</span>
            </div>
            <div class="content">
                Ya, untuk melakukan pemesanan kamu harus terdaftar sebagai member Laperbanget.com. Proses registrasi sebagai member mudah banget dan kamu akan mendapatkan banyak keuntungan dan kemudahan, seperti promosi, diskon, ataupun promosi bebas delivery charge.
            </div>
			
            <div class="title">
                <span class="stabilo">Adakah persyaratan khusus untuk menjadi member?</span>
            </div>
            <div class="content">
                Tidak, kamu hanya perlu mengisi informasi yang diperlukan. Untuk seorang member yang pertama kali melakukan pemesanan, kami hanya memverifikasi nomor telepon kamu, verifikasi dilakukan dengan mengontak nomor yang bersangkutan.  Hal ini perlu dilakukan untuk menjaga kenyamanan dan keamanan kamu bertransaksi.
            </div>
            <div class="title">
                Berapa biaya yang diperlukan untuk menjadi member?
            </div>
            <div class="content">
                Gratis!! kamu gak perlu merogoh kocek sepeser pun untuk bisa jadi member laperbanget.com!!
            </div>
            <div class="title">
                Bagaimana saya dapat bergabung menjadi partner/mitra Laperbanget.com?
            </div>
            <div class="content">
               Menjadi partner kami sangatlah mudah. Bila restoran/rumah makan/warung Anda berada di salah satu area operasional kami, Anda tinggal mengisi form ini. Lalu salah satu Sales Representative kami akan menghubungi kamu.
            </div>
			<div class="title">
                Apa keuntungan yang diberikan oleh laperbanget.com kepada restoran saya?
            </div>
            <div class="content">
            - Dengan semakin besarnya pertumbuhan pengguna internet di Indonesia (khususnya mahasiswa), maka pasar untuk layanan online food order delivery ini akan semakin besar.
			- laperbanget.com akan meningkatkan pesanan menu restoran kamu melalu pelayanan kami sehingga tentunya omset kamu akan bertambah besar
			- laperbanget.com menyediakan Dashboard khusus yang menampilkan statistik dari beberapa pemesanan, statistik ini disuguhkan dalam bentuk grafik yang NAH.. memukau nan memesona, sehingga kamu tak akan pernah berpaling darinya(sehingga akan memudahkan owner restaurant untuk memantau cash-flow keuangan serta rekapitulasi order di bagian delivery. 
			- Efektifitas dalam proses delivery di lapangan.
            </div>
        </div>
        </div>
        <footer>
           
            <div class="wrapper">
                <div class="copyright left">
                Copyright © laperbanget 2013  
                </div>
                 <div class="fanpage-like left">
                    <div class="fb-like" data-href="https://www.facebook.com/laperbangetID" data-send="false" data-width="300" data-show-faces="true"  data-colorscheme="dark"></div>
                </div>
                <div class="twitter-follow left">
<a href="https://twitter.com/laperbangetID" class="twitter-follow-button" data-show-count="false">Follow @laperbangetID</a>
                    
                </div>
                
            </div>
        </footer>
        <?php $this->load->view('google_analytics'); ?>
        <script>!function(d,s,id){
    var js,fjs=d.getElementsByTagName(s)[0];
    if(!d.getElementById(id)){
        js=d.createElement(s);js.id=id;
        js.src="//platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js,fjs);}}
        (document,"script","twitter-wjs");
        </script>
  </body>

</html>