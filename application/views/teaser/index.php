<!DOCTYPE html>
<HTML lang="en">

  <HEAD>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=IE7">
    
    <meta name="keywords" content="food delivery bandung, jatinangor, order food online, delivery service, bandung, jatinangor, cileunyi, cinunuk, food, restaurant delivery, laperbanget, laparbanget,lapar,banget, hungry, pesan makan, pesan, makan" />
    <meta name="robots" content="noodp,noydir" />
    <meta name="allow-search" content="yes" />
    <meta name="audience" content="all" />
    <meta name="distribution" content="global" />
    <meta name="document-classification" content="general" />
    <meta name="rating" content="general" />
    
	<!--
    <meta property="description" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor. " />
    <meta name="description" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor. " />
    <meta property="name" content="laperbanget? kamu pesen, kami anter. Order food for delivery in Jatinangor." />
	-->
    
    <meta property="fb:admins" content="1394565663"/>
    <!-- <meta property="fb:page_id" content="216747471668883"/> -->
    <meta property="og:image" content="http://www.laperbanget.com/assets/teaserasset/img/transparent_icon_200.png"/>
    <meta property="og:title" content="Laperbanget.com | Online Food Order Delivery System" />
    <meta property="og:description" content="laperbanget? kamu pesen, kami anter. Online Food Order Delivery in Jatinangor. " />
	
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?php echo $title ?>" />
    <meta property="og:url" content="http://www.laperbanget.com/teaser/"/>

   



    <title><?php echo $title ?></title>
    <link rel="icon"  type="image/png" href="<?php echo local_path('assets/teaserasset/img/logo.png'); ?>">
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/reset.css'); ?>" type="text/css" media="screen"> 
    <link rel="stylesheet" href="<?php echo local_path('assets/teaserasset/css/style.css'); ?> " type="text/css" media="screen"> 

    <!--- dropdown menu -->


    <!-- Add jQuery library -->
    <script type="text/javascript" src="<?php echo local_path('assets/js/jquery-1.8.2.min.js'); ?>"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo local_path('assets/js/fancybox/jquery.fancybox.js?v=2.1.1'); ?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo local_path('assets/js/fancybox/jquery.fancybox.css?v=2.1.1'); ?> " media="screen" />
    <script type="text/javascript">
    $(document).ready(function() {
	   var slided = false;
        $('.sign-in').click(function(){
            if(!slided){
                $('.sign-in').animate({ right:"240"} , 400);
                $('.sign-in-form').animate({ right:"240"} , 400);
                slided = true;
            }
            else{
                $('.sign-in').animate({ right:"0"} , 400);
                $('.sign-in-form').animate({ right:"0"} , 400);
                slided = false;
            }
        })




        $('.fancybox').fancybox(
            {
                padding: 0,

                openEffect : 'elastic',
                openSpeed  : 150,

                closeEffect : 'elastic',
                closeSpeed  : 150,

                prevEffect : 'elastic',
                nextEffect : 'elastic',
            });
		$(".button-mitra").click(function() {
			$.fancybox.open([
			{
				href : '<?php echo local_path("assets/teaserasset/img/logo-mitra/ayamlaos.png")?>',                
				title : 'Ayam Laos'
			},
			{
				href : '<?php echo local_path("assets/teaserasset/img/logo-mitra/checo")?>',                
                title : 'Che.co'
			},
			{
				href : '<?php echo local_path("assets/teaserasset/img/logo-mitra/dwiscar")?>',                
                title : 'Dwiscar'
			},
            {
                href : '<?php echo local_path("assets/teaserasset/img/logo-mitra/ikonyo")?>',                
                title : 'RM. Padang Ikonyo'
            },
            {
                href : '<?php echo local_path("assets/teaserasset/img/logo-mitra/shafa")?>',                
                title : 'Shafa Yogurt'
            },


			], {
				padding : 0
			});
			return false;
			});

                    $(window).scroll(function() {
                        if ($(window).scrollTop() > 20) {
                            $('header').addClass('fixed');
                        } else {
                            $('header').removeClass('fixed');
                        }
                    });
					/*
                 $("#fancybox-1").hover(function() {
						
                      $("#pesan-1").show();  
                    },
					
					function() {
					
                       $("#pesan-1").hide();
                    });
                  $("#fancybox-2").hover(function() {
                      $("#pesan-2").show();  
                    }, function() {
                       $("#pesan-2").hide();
                    });
                   $("#fancybox-3").hover(function() {
                      $("#pesan-3").show();  
                    }, function() {
                       $("#pesan-3").hide();
                    });
                    $("#fancybox-4").hover(function() {
                      $("#pesan-4").show();  
                    }, function() {
                       $("#pesan-4").hide();
                    });
                     $("#fancybox-5").hover(function() {
                      $("#pesan-5").show();  
                    }, function() {
                       $("#pesan-5").hide();
                    });
                      $("#fancybox-6").hover(function() {
                      $("#pesan-6").show();  
                    }, function() {
                       $("#pesan-6").hide();
                    });
					*/
    });
    </script>

  </HEAD>

  <BODY>
   <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=144803309006640";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	
    <!-- FIXED DIV FORM untuk SIGN IN-->
	<!--
    <div class="sign-in"><a href="#" class="left">SIGN IN</a></div>
    <div class="sign-in-form">
        <?php echo form_open('home/verify_login'); ?>
            <div>
                <label>Email / Handphone</label>
                <input type="text" name="email_handphone" value=""/>
            </div>
            <div>
                <label>Password</label>
                <input type="password" name="password" value=""/>
            </div>
            <input type="checkbox" class="left" name="rememberme"/>
            <span class="left remember-me">Remember me</span>
            <a href="#" class="left" id="register">Belum punya akun?</a>
            <input type="submit" class="right" id="login-btn"/>
        </form>
    </div>
	-->

    <header>
      <div class="wrapper">
        <div id="logo">
          <img src="<?php echo local_path('assets/teaserasset/img/logo.png'); ?>" alt="" />
        </div>
        <div id="top-nav"> 
          <ul id="nav-menu">
            <li> <a href="<?php echo local_path('home/faq'); ?>"><span class="yellow-bold">Apakah itu </span> laperbanget ? </a></li>
            <!--<li> <a href="#"><span class="yellow-bold">Lihat katalog </span>menu</a> </li>-->
            <li> <a href="<?php echo local_path('home/faq'); ?>"><span class="yellow-bold">Ingin</span>menjadi Mitra ? </a></li>
			<li> <a href="<?php echo local_path('home/daftar'); ?>"><span class="yellow-bold">Segera </span>Daftar!!! </a></li>
		  </ul>
        </div>
        
        <!--
          <div class="fb-btn right">
              <div class="fb-like" data-href="http://laperbanget.com/teaser/" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="segoe ui" data-action="recommend"></div>


          </div>   
          <div class="twitter-btn right">
             <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://laperbanget.com" data-text="Jatinangor sekarang bisa delivery dimana saja" data-via="laperbanget" data-hashtags="laperbanget"
             >Tweet</a>
          </div>   
          --> 
     
    </div>
    </header>
     <div id="header-cover-img">
	 <div>
            <img src="<?php echo local_path('assets/teaserasset/img/header_teaser.png'); ?>" alt="">
			</div>
    </div>
    
    <div id="container">
        <div  class="wrapper2 wrapper">
        <div id="reward_icon">
            <img src="<?php echo local_path('assets/teaserasset/img/icon_reward.png'); ?>" alt="" />
        </div>
        <div id="registration">
		<h2>Segera Subscribe!!</h2>
            <h3>Dapatkan informasi menarik dari kami!!</h3>

            <p>Caranya, tinggal klik tombol "subsribe" di bawah ini, lalu ikuti petunjuk selanjutnya :D</p>
			<!--
            <h2>Segera subscribe di laperbanget.com!!</h2>
            <h3>& pantau terus informasi dari kami!</h3>

            <p>Hayoo banyak informasi - informasi penting yan</p>-->
        </div>
        <div class="registration-button">
            <button type="submit" onClick="javascript:window.location='<?php echo local_path('home/daftar'); ?>'">Subscribe</button>
        </div>        
    </div>
    </div>
<!--
    <form id="searchbox">
        <input id="search" type="text" placeholder="Type here" size="35">
        
        <div>
            <a href="#"><img src="search-img.png" alt=""/></a>
        </div>
    </form>
   

    <div id="search-button" class="wrapper">
        <div class="text">
            <input name="q" type="text" size="30" value="Cari menu makanan / minuman">
        </div>
        <div>
            <a href="#"><img src="search-img.png" alt=""/></a>
        </div>
    </div>

-->

    <div id="menu-makanan" >
        <div class="wrapper2 wrapper">
        <ul class="list-menu">
            <li>
                <div class ="menunya">
                    <a class="fancybox" id="fancybox-1" href="<?php echo local_path('assets/teaserasset/img/menu1.jpg'); ?>" data-fancybox-group="gallery" title="Es Buah Dwiscar">
                        <div class="shadow"></div>
                        <img src="<?php echo local_path('assets/teaserasset/img/menu1.jpg'); ?>" alt="" width="255" height="255" /></a>
                    <a class="pesan-btn" id="pesan-1" href="#">LIHAT</a>
                    <div class="explanation">
                        <h4>Es Buah</h4>
                        <p>@Dwiscar</p>
                    </div>
                    <div class="menu-button">
                        <button type="submit">+60</button>
                    </div>
                </div>
            </li>
            <li>
                <div class ="menunya">
                    <a class="fancybox" id="fancybox-2" href="<?php echo local_path('assets/teaserasset/img/menu2.jpg'); ?>" data-fancybox-group="gallery" title="Rendang | RM. Padang Ikonyo">
                    <div class="shadow"></div>

                    <img src="<?php echo local_path('assets/teaserasset/img/menu2.jpg'); ?>" alt="" width="255" height="255" /></a>
                    <a class="pesan-btn fancybox" id="pesan-2" href="<?php echo local_path('assets/teaserasset/img/menu2.jpg'); ?>" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">LIHAT</a>
                    <div class="explanation">
                        <h4>Rendang</h4>
                        <p>@RM. Padang - Ikonyo</p>
                    </div>
                    <div class="menu-button">
                        <button type="submit">+60</button>
                    </div>
                </div>
            </li>
            <li>
                <div class ="menunya">
                    <a class="fancybox" href="<?php echo local_path('assets/teaserasset/img/menu3.jpg'); ?>" id="fancybox-3" data-fancybox-group="gallery" title="Regular Yogurt |  @Shafa Yogurt">
                        <div class="shadow"></div>
                        <img src="<?php echo local_path('assets/teaserasset/img/menu3.jpg'); ?>" alt="" width="255" height="255" /></a>
 <a class="pesan-btn" id="pesan-3" href="#">LIHAT</a>
                    <div class="explanation">
                        <h4>Regular Yogurt</h4>
                        <p> @Shafa Yogurt</p>
                    </div>
                    <div class="menu-button">
                        <button type="submit">+60</button>
                    </div>
                </div>
            </li>
        </ul>
        <ul>
            <li>
                <div class ="menunya">
                    <a class="fancybox" id="fancybox-4" href="<?php echo local_path('assets/teaserasset/img/menu4.jpg'); ?>" data-fancybox-group="gallery" title="Deng - Deng Sambel Ijo | @RM. Padang - Ikonyo">
                        <div class="shadow"></div>
                        <img src="<?php echo local_path('assets/teaserasset/img/menu4.jpg'); ?>" alt="" width="255" height="255" /></a>
                         <a class="pesan-btn" id="pesan-4" href="#">LIHAT</a>
                    <div class="explanation">
                        <h4>Deng - Deng Sambel Ijo</h4>
                        <p>@RM. Padang - Ikonyo</p>
                    </div>
                    <div class="menu-button">
                        <button type="submit">+60</button>
                    </div>
                </div>
            </li>
            <li>
                <div class ="menunya">
                    <a class="fancybox" id="fancybox-5" href="<?php echo local_path('assets/teaserasset/img/menu5.jpg'); ?>" data-fancybox-group="gallery" title="Juice Alpuket | @Dwiscar">
                    <div class="shadow"></div>
                        <img src="<?php echo local_path('assets/teaserasset/img/menu5.jpg'); ?>" alt="" width="255" height="255" /></a>
                         <a class="pesan-btn" id="pesan-5" href="#">LIHAT</a>
                    <div class="explanation">
                        <h4>Juice Alpuket</h4>
                        <p> @Dwiscar</p>
                    </div>
                    <div class="menu-button">
                        <button type="submit">+60</button>
                    </div>
                </div>
            </li>
            <li>
                <div class ="menunya">
                    <a class="fancybox" id="fancybox-6" href="<?php echo local_path('assets/teaserasset/img/menu6.jpg'); ?>" data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                        <div class="shadow"></div>
                        <img src="<?php echo local_path('assets/teaserasset/img/menu6.jpg'); ?>" alt="" width="255" height="255" /></a>
                         <a class="pesan-btn" id="pesan-6" href="#">LIHAT</a>
                    <div class="explanation">
                        <h4> Nama makanan / minuman</h4>
                        <p> @ cafe apa </p>
                    </div>
                    <div class="menu-button">
                        <button type="submit">+60</button>
                    </div>
                </div>
            </li>
        </ul>
		
        <div id="katalog-button">
            <button type="submit" class="button-mitra">Partnert Laperbanget</button>
        </div>
		
    </div>
    </div>
        <footer class="bottom-fixed">
           
            <div class="wrapper">
                <div class="copyright left">
                Copyright © laperbanget 2013  
                </div>
				
                 <div class="fanpage-like left">
                    <div class="fb-like" data-href="https://www.facebook.com/laperbangetID" data-send="false" data-width="300" data-show-faces="true"  data-colorscheme="dark"></div>
                </div>
                <div class="twitter-follow left">
<a href="https://twitter.com/laperbangetID" class="twitter-follow-button" data-show-count="false">Follow @laperbangetID</a>
                    
                </div>
                
            </div>
			
        </footer>

<script>!function(d,s,id){
    var js,fjs=d.getElementsByTagName(s)[0];
    if(!d.getElementById(id)){
        js=d.createElement(s);js.id=id;
        js.src="//platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js,fjs);}}
        (document,"script","twitter-wjs");
        </script>
 <script type="text/javascript">
  $(document).ready(function() {
		<?php if ($this->session->flashdata('email'))
		{
			echo 'alert("'.$this->session->flashdata('email').'");';
		}
		?>
		});


 
 </script>

        <?php $this->load->view('google_analytics'); ?>
  </BODY>

</HTML>