<?php $this->load->view('administrator/header'); ?>
	<div class="container-fluid">
		<div class="row-fluid">
		<?php if(!isset($no_visible_elements) || !$no_visible_elements) { ?>
		
			<!-- left menu starts -->
			<?php 

			$this->load->view('administrator/leftmenu'); 
			$this->load->view('administrator/warning/no_javascript_warning');  // warning klo javascript di non aktifkan
			 ?>
			<!-- left menu ends -->
			
			
			
			<div id="content" class="span10">
			<!-- content starts -->
			<?php } ?>


			<!-- navigasi starts here-->
			<?php $this->load->view('administrator/navigasi_content'); ?>
			<!-- end of navigasi -->
			<!-- content utama start here -->
			<?php $this->load->view($maincontent); ?>
			<!-- end of content utama -->
				  

		  
       
<?php $this->load->view('administrator/footer'); ?>
