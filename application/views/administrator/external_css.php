<link id="bs-css" href="<?php echo local_path('assets/css/bootstrap-cerulean.css'); ?>" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>

<link href="<?php echo local_path('assets/css/bootstrap-responsive.css'); ?>" rel="stylesheet">
	<link href="<?php echo local_path('assets/css/charisma-app.css'); ?>" rel="stylesheet">
	<link href="<?php echo local_path('assets/css/jquery-ui-1.8.21.custom.css'); ?>" rel="stylesheet">

	<link href="<?php echo local_path('assets/css/chosen.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/uniform.default.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/colorbox.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/jquery.cleditor.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/jquery.noty.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/noty_theme_default.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/elfinder.min.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/elfinder.theme.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/jquery.iphone.toggle.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/opa-icons.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/css/uploadify.css'); ?>" rel='stylesheet'>
	<link href="<?php echo local_path('assets/js/fancybox/jquery.fancybox.css?v=2.1.1'); ?>" rel='stylesheet'>
	<?php 

	foreach($grocery->css_files as $file): ?>
    	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
	<?php endforeach; ?>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->