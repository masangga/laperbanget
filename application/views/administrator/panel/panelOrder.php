<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Admin</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">

						  <thead>
						  	<a class="btn btn-success fancybox fancybox.ajax" href="<?php echo local_url('admin/addAdmin') ?>">
										<i class="icon-plus icon-white"></i>
										tambah
									</a>
							  <tr>
								  <th>Username</th>
								  <th>Date registered</th>
								  <th>Role</th>
								  <th>Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
						  	<?php foreach ($resultsadmin as $row) {
						  	?>
						  	
							<tr>
								<td><?php echo $row->nama_lengkap; ?></td>
								<td class="center"><?php echo $row->waktu_buat; ?></td>
								<td class="center"><?php echo $row->level; ?></td>
								<td class="center">
									<?php switch ($row->status) {
										case 'active':
											echo '<span class="label label-success">Active</span>';
											break;
										case 'inactive':
											echo '<span class="label">Inactive</span>';
											break;
										case 'banned':
											echo '<span class="label label-important">Banned</span>';
											break;
										case 'pending':
											echo '<span class="label label-warning">Pending</span>';
											break;	
										default:
											echo 'no data found';
											break;
									}
									?>
								</td>
								
								<td class="center">
									<a class="btn btn-success fancybox fancybox.ajax" href="<?php echo local_url('admin/viewAdmin').'/'.$row->id_admin; ?>">
										<i class="icon-zoom-in icon-white"></i>  
										View                                            
									</a>
									<a class="btn btn-info fancybox fancybox.ajax" href="<?php echo local_url('admin/editAdmin').'/'.$row->id_admin; ?>">
										
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>

									<a class="btn btn-danger" href="<?php echo local_url('admin/deleteAdmin').'/'.$row->id_admin; ?>" onclick="return confirm(\'Really Delete?\'):" >
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
									
								</td>
							</tr>
							<?php }?>							
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row table pertama--> 
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Add New Administrator</h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">

					</div>
				</div>
</div>


