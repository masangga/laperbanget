<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="<?php echo local_path('assets/js/jquery-1.7.2.min.js'); ?>"></script>
	<!-- jQuery UI -->
	<script src="<?php echo local_path('assets/js/jquery-ui-1.8.21.custom.min.js'); ?>"></script>
	<!-- transition / effect library -->
	<script src="<?php echo local_path('assets/js/bootstrap-transition.js'); ?>"></script>
	<!-- alert enhancer library -->
	<script src="<?php echo local_path('assets/js/bootstrap-alert.js'); ?>"></script>
	<!-- modal / dialog library -->
	<script src="<?php echo local_path('assets/js/bootstrap-modal.js'); ?>"></script>
	<!-- custom dropdown library -->
	<script src="<?php echo local_path('assets/js/bootstrap-dropdown.js'); ?>"></script>
	<!-- scrolspy library -->
	<script src="<?php echo local_path('assets/js/bootstrap-scrollspy.js'); ?>"></script>
	<!-- library for creating tabs -->
	<script src="<?php echo local_path('assets/js/bootstrap-tab.js'); ?>"></script>
	<!-- library for advanced tooltip -->
	<script src="<?php echo local_path('assets/js/bootstrap-tooltip.js'); ?>"></script>
	<!-- popover effect library -->
	<script src="<?php echo local_path('assets/js/bootstrap-popover.js'); ?>"></script>
	<!-- button enhancer library -->
	<script src="<?php echo local_path('assets/js/bootstrap-button.js'); ?>"></script>
	
	<!-- autocomplete library -->
	<script src="<?php echo local_path('assets/js/bootstrap-typeahead.js'); ?>"></script>
	<!-- tour library -->
	<script src="<?php echo local_path('assets/js/bootstrap-tour.js'); ?>"></script>
	<!-- library for cookie management -->
	<script src="<?php echo local_path('assets/js/jquery.cookie.js'); ?>"></script>

	<!-- data table plugin -->
	<script src='<?php echo local_path('assets/js/jquery.dataTables.min.js'); ?>'></script>
	<!-- library for fancybox -->
	<script src='<?php echo local_path('assets/js/fancybox/jquery.fancybox.js?v=2.1.1'); ?>'></script>
	
	<!-- chart libraries start -->
	<script src="<?php echo local_path('assets/js/excanvas.js'); ?>"></script>
	<script src="<?php echo local_path('assets/js/jquery.flot.min.js'); ?>"></script>
	<script src="<?php echo local_path('assets/js/jquery.flot.pie.min.js'); ?>"></script>
	<script src="<?php echo local_path('assets/js/jquery.flot.stack.js'); ?>"></script>
	<script src="<?php echo local_path('assets/js/jquery.flot.resize.min.js'); ?>"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="<?php echo local_path('assets/js/jquery.chosen.min.js'); ?>"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="<?php echo local_path('assets/js/jquery.uniform.min.js'); ?>"></script>
	<!-- plugin for gallery image view -->
	<script src="<?php echo local_path('assets/js/jquery.colorbox.min.js'); ?>"></script>
	<!-- rich text editor library -->
	<script src="<?php echo local_path('assets/js/jquery.cleditor.min.js'); ?>"></script>
	<!-- notification plugin -->
	<script src="<?php echo local_path('assets/js/jquery.noty.js'); ?>"></script>
	<!-- file manager library -->
	<script src="<?php echo local_path('assets/js/jquery.elfinder.min.js'); ?>"></script>
	<!-- star rating plugin -->
	<script src="<?php echo local_path('assets/js/jquery.raty.min.js'); ?>"></script>
	
	<!-- autogrowing textarea plugin -->
	<script src="<?php echo local_path('assets/js/jquery.autogrow-textarea.js'); ?>"></script>
	<!-- multiple file upload plugin -->
	<script src="<?php echo local_path('assets/js/jquery.uploadify-3.1.min.js'); ?>"></script>
	<!-- history.js'); ?> for cross-browser state change on ajax -->
	<script src="<?php echo local_path('assets/js/jquery.history.js'); ?>"></script>
	<!-- application script for Charisma demo -->
	<script src="<?php echo local_path('assets/js/charisma.js'); ?>"></script>

	<?php foreach($grocery->js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>

	<script type="text/javascript">
	
	function myFunction()
	{
		var x;
		var confirmButton=confirm("Are you sure you want to delete this item?");
		if (confirmButton==true)
  		{
  			return true;
  		}
		else
  		{
  			return false;
  		}
  	}
		$(document).ready(function($) {
		// Stuff to do as soon as the DOM is ready;
		$('.fancybox')
		//.attr('rel', 'gallery')
		.fancybox({
			padding : 0,
		maxWidth    : 800,
        maxHeight   : 800,
        scrolling	: 'no',
        autoResize	: true,
        autoSize    : true,
        closeClick  : false,
        openEffect  : 'fade',
        closeEffect : 'fade'
		});
		
	});

	</script>