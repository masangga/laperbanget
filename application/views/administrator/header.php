<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<!-- The styles -->
	<link id="bs-css" href="<?php echo local_path('assets/css/bootstrap-cerulean.css'); ?>" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<?php
	$this->load->view('administrator/external_css');  //load all css libraries/styles
	?>

	<!-- The fav icon -->
	<link rel="shortcut icon" href="<?php echo local_path('assets/img/favicon.ico'); ?>">
		
</head>

<body>
	<?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
	<!-- topbar starts -->
	<?php $this->load->view('administrator/topbar'); ?>
	<!-- topbar ends -->
	<?php } ?>

