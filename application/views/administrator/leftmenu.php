<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="<?php echo local_url('admin'); ?>"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelAdmin'); ?>"><i class="icon-user"></i><span class="hidden-tablet"> Panel Admin</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelUser'); ?>"><i class="icon-user"></i><span class="hidden-tablet"> Panel User</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelTeaser'); ?>"><i class="icon-star"></i><span class="hidden-tablet"> Panel Teaser</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelOrder'); ?>"><i class="icon-shopping-cart"></i><span class="hidden-tablet"> Panel Order</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelMenu'); ?>"><i class="icon-picture"></i><span class="hidden-tablet"> Panel Menu Makanan</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelRestaurant'); ?>"><i class="icon-list-alt"></i><span class="hidden-tablet"> Panel Restaurant</span></a></li>

						<li><a class="ajax-link" href="<?php echo local_url('admin/panelKurir'); ?>"><i class="icon-font"></i><span class="hidden-tablet"> Panel Kurir</span></a></li>

						


						<li class="nav-header hidden-tablet">Sample Section</li>

					</ul>
					<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
				</div><!--/.well -->
			</div><!--/span-->