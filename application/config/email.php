<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class email
	{
		function __construct()
            {
                //include (base_url(). "/functions/config.php");
                parent::__construct();
				$this->load->library('email');
				$this->load->helper('email');
                $config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$this->email->initialize($config);
                //error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
            }
	}

?>
