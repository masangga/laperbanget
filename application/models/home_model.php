<?php
/**
* model yang khusus menangani bagian controller home
*/
class Home_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function insert($data)
	{
		$this->db->insert('member',$data);
	}

	function generate_select_result($sql)
	{
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row){
				$data[] = $row;
			}
		}
		$q->free_result();
		return $data;
	}

	
	function login($username, $password)
	{

	}

	//update cookie
	function update_cookie($username, $value)
	{

	}

	function is_auth_token_exist($value)
	{

	}

	function retrieve_data($token)		///retrieve semua data berdasarkan $token yang ditemukan
	{

	}

	function create_member($activation_key)
    {

    }

	function is_exist_activation_key($activation_key)
	{

	}

	function is_exist($username)
	{

	}

	function is_exist_email($email)
	{

	}
}
?>