<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class User_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function login($email_handphone, $password)
    {
            $data = array();
            $email_handphone = strtolower($email_handphone);
            
            $password=md5($password);
            $sql = "SELECT * FROM tbl_user WHERE (email='".mysql_real_escape_string($email_handphone)."' OR phone='".mysql_real_escape_string($email_handphone)."' ) AND password='".mysql_real_escape_string($password)."'  limit 0,1";
			
            $query = $this->db->query($sql);
            
            if($query->num_rows() == 1)
                {
					
                    foreach ($query->result() as $row) 
                    {
                        $data[] = $row;
                    }
                    //cek ulang apakah sudah konfirmasi kalau signup via email
                    if ($row->status == 'yes')
                    {
                    	//jika sudah terkonfirmasi
                    	return $data;
                    	$query->free_result();
                    }
                    else
                    {
                    	//jika belum terkonfirmasi, hanya mengembalikan sebuah string
                    	return "not confirmed";
                    }
                       
                }
            else
                {
                    return false;
                }
    }

    function fb_login($email)
    {
            $data = array();
            $email_handphone = strtolower($email_handphone);
            
            $password=md5($password);
            $sql = "SELECT * FROM tbl_user WHERE email='".mysql_real_escape_string($email_handphone)."' limit 0,1";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows() == 1)
                {
                    
                    foreach ($query->result() as $row) 
                    {
                        $data[] = $row;
                    }
                    //cek ulang apakah sudah konfirmasi kalau signup via email
                    if ($row->status == 'yes')
                    {
                        //jika sudah terkonfirmasi
                        return $data;
                        $query->free_result();
                    }
                    else
                    {
                        //jika belum terkonfirmasi, hanya mengembalikan sebuah string
                        return "not confirmed";
                    }
                       
                }
            else
                {
                    return false;
                }
    }

    function twitter_login($twitter_id)
    {
        $data = array();
        $username = strtolower($twitter_id);
        $sql = "SELECT * FROM tbl_user WHERE twitter_id='".mysql_real_escape_string($twitter_id)."' limit 0,1";
        $query = $this->db->query($sql);
        if($query->num_rows() == 1)
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
            return $data;
            $query->free_result();
        }
        else
        {
            return false;
        }
    }

    function update_login($id_user, $counter)
	{
		//var_dump($counter);
		$bool = false;
		$timezone = "Asia/Jakarta";
		if (function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
		$now = date("Y-m-d H:i:s"); 
		$this->db->where('id_user',$id_user);
        $data = array(
        'login_time' => $now ,
        'counter' => $counter + 1,
        );
		if ($this->db->update('tbl_user',$data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    //update cookie
    function update_cookie($id_user, $value)
    {
        $bool = false;
        $sql = "UPDATE tbl_user SET hash_token = '$value' WHERE id_user='$id_user'";
        $query = $this->db->query($sql);
        if ($query)
        {
            $bool = true;
            return $bool;
        }
        return $bool;
    }

    function is_auth_token_exist($value)
    {
        $bool = false;
        $sql = "SELECT * FROM tbl_user WHERE hash_token = '$value'";
        $q = $this->db->query($sql);
        if($q->num_rows() > 0){
            $bool = true;
        }
        $q->free_result();
        return $bool;
    }
    
    function retrieve_data($token)      ///retrieve semua data berdasarkan $token yang ditemukan
    {
        $sql = "SELECT * FROM tbl_user WHERE hash_token = '$token'";
        
        $query = $this->db->query($sql);
        if($query->num_rows() == 1)
            {   
                foreach ($query->result() as $row) 
                {
                    $data[] = $row;
                }
                // var_dump($data);
                return $data;
                $query->free_result();
                }
            else
                {
                    return false;
                }
    }

    function create_new_member($data)
    {
        $query = $this->db->insert('tbl_user',$data);
        return $query;
    }

    function is_email_exist($email)
    {
        $bool = false;
        $sql = "SELECT * FROM tbl_user WHERE email = '$email'";
        $q = $this->db->query($sql);
        if($q->num_rows() > 0){
            $bool = true;
        }
        $q->free_result();
        return $bool;
    }

    function is_phone_exist($phone)
    {
        $bool = false;
        $sql = "SELECT * FROM tbl_user WHERE phone = '$phone'";
        $q = $this->db->query($sql);
        if($q->num_rows() > 0){
            $bool = true;
        }
        $q->free_result();
        return $bool;
    }

    function is_twitter_exist($twitter)
    {
        $bool = false;
        $sql = "SELECT * FROM tbl_user WHERE twitter_id = '$twitter'";
        $q = $this->db->query($sql);
        if($q->num_rows() > 0){
            $bool = true;
        }
        $q->free_result();
        return $bool;
    }

    function is_exist_activation_key($activation_key)
    {
        $bool = false;
        $sql = "select * from tbl_user where activation_key = '$activation_key'";
        $q = $this->db->query($sql);
        if($q->num_rows() > 0){
            $bool = true;
        }
        $q->free_result();
        return $bool;
    }
}