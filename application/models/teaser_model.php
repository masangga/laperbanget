<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teaser_model extends CI_Model {
	function __construct()
	{
		parent::__construct();

	}

	function is_exist_email($email)
	{
		$bool = false;
		$sql = "select * from tbl_teaser where email = '$email'";
		$q = $this->db->query($sql);
		if($q->num_rows() > 0){
			$bool = true;
		}
		$q->free_result();
		return $bool;
	}

	function is_exist_twitter_username($twitter_username)
	{
		$bool = false;
		$sql = "select * from tbl_teaser where twitter_username = '$twitter_username'";
		$q = $this->db->query($sql);
		if($q->num_rows() > 0){
			$bool = true;
		}
		$q->free_result();
		return $bool;
	}
	

}

/* End of file teaser_model.php */
/* Location: ./application/models/teaser_model.php */


?>