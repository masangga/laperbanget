<?php
/**
* 
admin model
*/
class admin_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();

	}

	function adminlogin($username, $password)
	{
		$data = array();
		$username = strtolower($username);   //lower username value
		$password=md5($password); //pwd in md5 mode
		$sql = "SELECT * FROM tbl_admin WHERE username='".mysql_real_escape_string($username)."' AND password='".mysql_real_escape_string($password)."'";  //sql query

		$query = $this->db->query($sql);
		//fetching data
		if($query->num_rows() == 1)
		{
			foreach ($query->result() as $row) 
			{
				$data[] = $row;
			}
			return $data;
			$query->free_result();
		}
		else
		{
			return false;
		}
	}

	//get email admin by id
	function get_email_admin_by_id($id_admin)
	{
		$sql = "SELECT email FROM tbl_admin WHERE id_admin = '$id_admin'";
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row){
				$email = $row->email;
			}
		}
		$q->free_result();
		return $email;
		//return $this->generate_select_result($sql);
	}
	

	//update login last IP
	function update_login_last_ip($id_admin, $last_login_ip)
	{
		$query = $this->db->where('id_admin',$id_admin);
		$data['last_login_ip'] = $last_login_ip;
		$this->db->update('tbl_admin',$data);
	}

	//generate result from sql
	function generate_select_result($sql){
		$q = $this->db->query($sql);
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row){
				$data[] = $row;
			}
		}
		$q->free_result();
		return $data;
	}

}
?>