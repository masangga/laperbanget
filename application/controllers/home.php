<?php
/**
*  class utama yang berfungsi menangani logik di bagian hom
*/
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$this->load->model('home_model',TRUE);
		$this->load->model('admin_model');
		$this->load->model('user_model');
	}

	//fungsi di halaman index
	public function index()
	{
		$token = $this->input->cookie('auth_token', TRUE);     ///retrieve token dalam cookies
		echo $token;
		
		if ($this->login_cookies($token))
		{ 
			redirect('utama', 'refresh');
			//echo 'ad cookie';
		}
		else
		{
			$this->landing();
		}
		
		//$this->landing();
		//redirect('teaser','refresh');
	}

	function daftar($retrieved_data)
	{
		//var_dump($retrieved_data);
		if ($retrieved_data)
		{
			$data['title'] = "Laperbanget.com | Online Food Order Delivery | Daftar";
			$this->load->view('utama/daftar',  array_merge($data, $retrieved_data) , FALSE);
		}
		else
		{
			$data['title'] = "Laperbanget.com | Online Food Order Delivery | Daftar";
			$this->load->view('utama/daftar',  $data , FALSE);
		}
	}

	function login()
	{
		$data['title'] = "Laperbanget.com | Online Food Order Delivery | Masuk";
		$this->load->view('utama/login', $data, FALSE);
	}
	function landing()
	{
		$data['title'] = "Laperbanget.com | Online Food Order Delivery";
		$this->load->view('utama/landing', $data);
	}

	function underconstruction()
	{
		$data['title'] = "laperbanget.com is under construction";
		$this->load->view('underconstruction', $data);
	}

	// halaman login admin
	public function login_admin()
	{
		
			$data['title'] = 'Halaman Login Admin'; //title page
			$data['welcome'] = "Selamat Datang di Halaman administrator laperbanget.com"; //welcome title
			$this->load->view('administrator/login/login_page', $data);

	}

	function login_cookies($token)
		{
		//var_dump($this->user_model->is_auth_token_exist($token));
			if ($this->user_model->is_auth_token_exist($token)); /// jika terdapat token yang serupa maka
			{
				$result = $this->user_model->retrieve_data($token);
				$session_array = array();
				//var_dump($result);
				if ($result)
				{
					foreach($result as $row)
					{
						$session_array = array(
						'id_user' => $row->id_user,
						'nama' => $row->nama,
						'counter' => $row->counter,
						'logged_in' => TRUE
						);
					}
					//update waktu login dan jumlah counter
					$this->user_model->update_login($session_array['id_user'], $session_array['counter']); 
					$this->session->set_userdata('user', $session_array);
					return true;
				} 
			
			}
			return false;
		
		}

	//fungsi untuk memverifikasi login versi Admin
	public function adminverify()
    {

        
		$data['title'] = 'Validasi Login';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|callback_check_database_admin['.$this->input->post('username').']');
		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  User redirected to login page
			// $this->load->view('login_validation',$data); 
			$this->index();
		}
		else
		{
			//echo 'berhasil';
			redirect('admin', 'refresh');
		}
		
	}

	//cek database admin versi admin
	public function check_database_admin($password,$username)
	{
		//query the database
		$result = $this->admin_model->adminlogin($username, $password);	
		// echo ($result[status]);
		if($result)
		{
			//masukin session dalam sebuah variabel biar bisa diakses
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array
				(
					'id_admin' => $row->id_admin,
					'username' => $row->username,
					'level' => $row->level,
					'email' => $row->email,
					'last_login_ip' => $row->last_login_ip
				);
			} 
			$test = $this->admin_model->update_login_last_ip($sess_array['id_admin'], $this->input->ip_address());
			//var_dump($test);
			$this->session->set_userdata('admin', $sess_array);
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password ');
			$this->data['error'] = true;
			return false;
		}     
	}


	//fungsi untuk memverifikasi login user
	public function verify_login()
    {
    	//var_dump($this->input->post());
        
		$data['title'] = 'Validasi Login';
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email_handphone', 'Email dan Handphone', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|callback_check_database');
		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  User redirected to login page

			$this->index();
		}
		else
		{
			//echo 'berhasil';
			redirect('utama', 'refresh');
		}
		
		
	}

	public function check_database($password)
	{

		//Field validation succeeded.  Validate against database
		$email_handphone = $this->input->post('email_handphone');
		$rememberme = $this->input->post('rememberme');
		//query the database
		$result = $this->user_model->login($email_handphone, $password);

		
	
		//jika user belum konfirmasi email
		if ($result == 'not confirmed')
		{
			$this->statConfirmed=false;
			return false;
		}
		//jika user sudah konfirmasi
		else if ($result)
		{
			$session_array = array();
			foreach($result as $row)
			{
				$session_array = array
				(
					'id_user' => $row->id_user,
					'nama' => $row->nama,
					'counter' => $row->counter,
					'logged_in' => TRUE
				);
			}
			//update waktu login dan jumlah counter
			$this->user_model->update_login($session_array['id_user'], $session_array['counter']); 

			//jika checkbox rememberme = ok
			if ($rememberme == 'on')
			{
				$test = $this->save_cookies($session_array['nama'], $session_array['id_user']);
				
			}
			$this->session->set_userdata('user', $session_array);
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid email/phone or password ');
			$this->data['error'] = true;
			return false;
		}
		
	}



	function doDaftar($error='')
	{
		//var_dump($this->input->post());
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama','Nama','trim|required|');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
		$this->form_validation->set_rules('handphone','Phone','trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean|min_length[4]');
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required|xss_clean');
		$this->form_validation->set_rules('terms', 'Terms and Condition', 'required');
		$run = $this->form_validation->run();
		//cek kalau email atau nomor hp ternyata sudah ada

		//$test = $this->user_model->is_email_exist($this->input->post('email')); 
		//$test = $this->user_model->is_phone_exist($this->input->post('handphone')); 
		//var_dump(!$test);
		
		if ($run == true && !( $this->user_model->is_email_exist($this->input->post('email')) || $this->user_model->is_phone_exist($this->input->post('handphone')) ) )
		{
			//validasi berhasil
			$dataInput = 
			array(
				'nama' => $this->input->post('nama'),
				'phone' => $this->input->post('handphone'),
				'email'     => $this->input->post('email'),
				'alamat'     => $this->input->post('alamat'),
				'password' => (md5($this->input->post('password'))),
				'activation_key' => $this->create_activation_key(),
				'login_time' => 'NOW()'
        				);
			var_dump($this->user_model->create_new_member($dataInput));
			//masuk ke halaman terima kasih sudah mendaftar
			//kirimkan email konfirmasi ke email user
		}
		else
		{
			//validasi gagal
			echo validation_errors();
			echo 'ada yang gagal';
			//ke halaman serupa
			//munculkan letak kesalahan
		}
		
	}


	//fungsi untuk mengsave cookies
	function save_cookies($nama, $id_user)
	{
		$hash = $this->get_token($nama);
		// save cookies
		$cookie = array
			(
			'name'   => 'auth_token',
			'value'  => $hash,
			'expire' => 1209600,  // Two weeks
			);
		$test = $this->input->set_cookie($cookie,true);
		$this->user_model->update_cookie($id_user, $hash);
	}

	//mengenerate random token dari name
	function get_token($nama)
	{
		$temp_token = '';
		$length = 8;
		$p = 0;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';                    
        for ($p = 0; $p < $length; $p++) 
		{
			$temp_token .= $characters[mt_rand(0, strlen($characters))];
		}

		$randomString = '';
    	for ($i = 0; $i < $length; $i++)
    	{
        	$randomString .= $nama[rand(0, strlen($nama) - 1)];
    	}
		$temp_token .= $randomString;
		return $temp_token;
	}

	function send_email_confirmation($useremail, $activation_key)
	{
		$this->load->library('email');
		$this->load->helper('email');
		if (valid_email($useremail))
        {
			$this->email->from('no-reply@vrala.com', 'Validasi');
			$this->email->to($useremail);
			//$this->email->cc('another@example.com');
			//$this->email->bcc('and@another.com');
			$this->email->subject('Email Confirmation');
			$this->email->message($this->create_confirmation_content($activation_key));
			$this->email->send();
			echo $this->email->print_debugger();
		}
	}

	//isi pesan konfirmasi email (ada activation key nya)
	function create_confirmation_content($activation_key)
	{
		$link_message = local_path("home/account_activation/".$activation_key); //belum jadi link
		return $link_message;
	}

	function create_activation_key()
	{
		//membuat aktivasi key
		$length = 10; 
		$activationkey = '';
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';                    
		for ($p = 0; $p < $length; $p++) 
		{
			$activationkey .= $characters[mt_rand(0, strlen($characters))];
		}      
		return $activationkey;  
	}

	function account_activation()
	{
		$activation_key = $this->uri->segment(3);
		$bool = $this->home_model->is_exist_activation_key($activation_key);
		if ($bool)
		{
			//ada activation key, pernah terdaftar
			echo ('teraktivasi');
			$this->home_model->update_confirmation($activation_key);
		}
		else
		{
			echo ('tidak ada kode aktivasi');
		}
	}

	
        function signup_with_twitter()
        {
            //signup via twitter
        	$this->load->library('twitter');
            if($this->twitter->is_logged_in())
            {
                $user = $this->twitter->call('get', 'account/verify_credentials');
                //var_dump($user);
                $username = $user['screen_name'];
				$picture = $user['profile_image_url'];
				$nama = $user['name'];
                $data = array(

                                'username' => $user['screen_name'],
                                'nama' => $user['name'],
                                'picture' => $user['profile_image_url'],
                                'tipe' => 'twitter',
                                'login_time' => 'NOW()'
                 );
                
                // cek database apakah username sudah ada atau belum
                $username_exist = $this->user_model->is_twitter_exist($data['username']);

                
                // apakah username sudah ada atau belu>
                if ($username_exist)
                {

                    //jika sudah ada
                    //berarti langsung masuk
                    //kemudian ambil data di datbase untuk dimasukkan dalam session
                    $user_details = $this->user_model->twitter_login($data['username']);
                    
                    //jika query berjalan lancar
                    if ($user_details)
                    {
                        //masukin session dalam sebuah variabel biar bisa diakses
                        $session_array = array();
						foreach($result as $row)
						{
							$session_array = array
							(
								'id_user' => $row->id_user,
								'nama' => $row->nama,
								'counter' => $row->counter,
								'logged_in' => TRUE
							);
						}
						//update waktu login dan jumlah counter
						$this->user_model->update_login($session_array['id_user'], $session_array['counter']); 
                        //session setting
                        $this->session->set_userdata('user', $session_array);
                        redirect('', 'refresh');
                    }
                    else
                    {
                        //jika query tidak berjalan dengan semestinya
                        die('error sql');
                    }
                }
                else
                {
                    //jika username ternyata tidak ada
                    //buat akun baru, kembali ke menu register
                    //form diisi kembali dengan data2 yang sudah diambil

                	//ada dua cara, pertama dengan fungsi parameter
                	//$this->daftar($data);

                	// kedua lewat session flash data
                	$this->session->set_flashdata('nama', $data['nama']);
                    redirect('home/daftar', 'refresh');
                }
            }
            else 
            {
                // no login
                $this->twitter->login();
            }
            
        }

	 //twitter authentication
	function twitter_authentication()
	{
		/* Are we logged in */
		if($this->twitter->is_logged_in())
		{
			/* Yes - go to main page */
			redirect(site_url('home/signup_with_twitter','refresh'));
		} 
		else 
		{
			/* No - login */
			$this->twitter->login();
		}
	}

	public function facebook_daftar()
	{
		$this->load->library('fbconnect');

		$data['redirect_uri'] = local_path('home/handle_facebook_masuk');
		$data['scope'] = 'email';
		//scope: 'email,read_stream,publish_stream,user_birthday,user_location,user_work_history,user_hometown,user_photos
		redirect($this->fbconnect->getLoginUrl($data));
	}

	public function handle_facebook_masuk()
	{
		$this->load->library('fbconnect');
		$this->load->model('admin_model');
		$facebook_user = $this->fbconnect->user;
		//var_dump($facebook_user);
		
		if ($this->fbconnect->user)
		{
			if ($this->user_model->is_email_exist($facebook_user['email']))
			{
				// jika email sudah ada di table user
				// langsung login saja
				$result = $this->user_model->fb_login($facebook_user['email']);	
				//jika user belum konfirmasi email
				if ($result == 'not confirmed')
				{
					$this->statConfirmed=false;
					return false;
				}
				//jika user sudah konfirmasi
				else if ($result)
				{
					$session_array = array();
					foreach($result as $row)
					{
						$session_array = array
						(
							'id_user' => $row->id_user,
							'nama' => $row->nama,
							'counter' => $row->counter,
							'logged_in' => TRUE
						);
					}
				//update waktu login dan jumlah counter
				$this->user_model->update_login($session_array['id_user'], $session_array['counter']); 

				$this->session->set_userdata('user', $session_array);
				return TRUE;
				}
				echo 'berhasil';
			}
			else
			{
				//var_dump($facebook_user['email']);
				// jika email belum ada di database user
				// kembali ke halaman register
				// isi form
				$this->session->set_flashdata('email', $facebook_user['email']); 
				$this->session->set_flashdata('nama', $facebook_user['name']); 
				redirect('home/daftar', 'refresh');
				// masukkan ke dalam database

				//$data['facebook_id'] = $facebook_user['id'];
				//$data['fb_link'] = $facebook_user['link'];
			}
		}
		else
		{
			$this->session->set_flashdata('email', 'error while daftar');
			redirect('home/daftar', 'refresh');
		}
		
	}

}
?>