<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
controller admin
*/
class Admin extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model','',TRUE);
		$this->load->library('grocery_CRUD');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		if ($this->session->userdata('admin'))
		{
			$this->session_data['admin'] = $this->session->userdata('admin');
		}
		else
		{
			redirect('home/login_admin');
		}

		
	}

	// halaman index
	function index()
	{
		
			
			$data['title'] = 'Halaman Dashboard Administrator';
			$data['navbreadcrumb'] = 'Dashboard';
			$data['maincontent'] = 'administrator/content/index_content';
			//echo $session_data['username'];
			$this->load->view('administrator/index', array_merge($data, $this->session_data) , FALSE);
		
	}


	function panelAdmin()
	{
		$this->session_data['admin'] = $this->session->userdata('admin');
		$data['title'] = 'Halaman Setting Admin';
		$data['navbreadcrumb'] = 'Setting Admin';
		$data['maincontent'] = 'administrator/panel/panelAdmin';
		$crud = new grocery_CRUD();
		$crud->set_table('tbl_admin');

		$crud->unset_add_fields('id_admin');
		$crud->required_fields('username', 'nama_lengkap', 'level', 'email','password');
		$crud->add_action('Send Email', local_path('assets/img/lb.png'), 'admin/send_email','fancybox fancybox.ajax');
		
		$crud->change_field_type('password','password');
		$crud->change_field_type('level', 'enum', array('top','low'));
		$crud->change_field_type('status', 'enum', array('active','inactive','pending', 'banned'));
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
  		$crud->callback_before_update(array($this,'encrypt_password_callback'));
		$crud->callback_edit_field('password',array($this,'decrypt_password_callback'));
		$data['grocery'] = $crud->render();
 		
 		//var_dump($output->js_files);
        $this->load->view('administrator/index', array_merge($data, $this->session_data));
	}

	
	function encrypt_password_callback($post_array, $primary_key = null)
	{
  		$this->load->library('encrypt');
  		$key = 'md5';
  		//$post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
  		$post_array['password'] = md5($post_array['password']);
  		return $post_array;
	}

	function decrypt_password_callback($value)
	{
  		$this->load->library('encrypt');
  		$key = 'md5';
  		//$decrypted_password = $this->encrypt->decode($value, $key);
  		$decrypted_password = md5($value);
  		return "<input type='password' name='password' value='$decrypted_password' />";
	}
	
	function send_email()
	{
		$id = $this->uri->segment(3);
		if ($id)
		{
			$data['email'] = $this->admin_model->get_email_admin_by_id($id);
			$data['maincontent'] = 'administrator/content/send_mail';
			$this->load->view('administrator/content/contentCrud', $data, FALSE);
		}
		else
		{
			//var_dump($this->input->post());
			// load form validation class
    		$this->load->library('form_validation');

			// set form validation rules
    		$this->form_validation->set_rules('sender_name', 'From', 'required');
			$this->form_validation->set_rules('sender_email', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('subject', 'Subject', 'required');
			$this->form_validation->set_rules('message', 'Message', 'required');
			$this->form_validation->set_rules('to', 'to', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				//redirect('admin/send_email', 'refresh');
				redirect('admin/panelAdmin');
			}
			else 
			{
				// you can also load email library here
				$this->load->library('email');
				$this->load->helper('email');
				// set email data
				
	    		$this->email->from($this->input->post('sender_email'), $this->input->post('sender_name'));
	    		$this->email->to($this->input->post('to'));
	    		$this->email->reply_to($this->input->post('sender_email'), $this->input->post('sender_name'));
	    		$this->email->subject($this->input->post('subject'));
	    		$this->email->message($this->input->post('message'));
	    		$this->email->send();
	    		echo $this->email->print_debugger();
				// create a view named "succes_view" and load it at the end
	    		redirect('admin/panelAdmin','refresh');
	    		//$this->load->view('success_view');
	    		
			}    	
		}
		
	}

	//expoxt to excel all admin data
	function export_excel_admin()
	{
		//$data['resultsadmin'] = $this->admin_model->get_all_data_admin();
		//var_dump($data['resultsadmin']);
		//$this->load->view('administrator/export_excel/export_excel_admin', $data);
		$query = $this->db->get('tbl_admin');
 		
        if(!$query)
            return false;
 
        // Starting the PHPExcel library
        $this->load->library('excel');
        $this->load->library('PHPexcel/IOFactory');
 		
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
 
        // Fetching the table data
        $row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
 
            $row++;
        }
 
        $objPHPExcel->setActiveSheetIndex(0);
 
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
 
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Products_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
 
        $objWriter->save('php://output');
       
	}


	function panelUser()
	{
		
		$data['title'] = 'Halaman Dashboard Administrator | Panel User';
		$data['navbreadcrumb'] = 'Panel User';
		$data['maincontent'] = 'administrator/panel/panelUser';
		$crud = new grocery_CRUD();
		$crud->set_table('tbl_user');
		$crud->set_subject('User');
		$crud->unset_add_fields('id_user');
		$crud->unset_edit_fields('id_user','password');
		$crud->set_field_upload('picture', 'assets/uploads/user/');
		$crud->change_field_type('password','password');
		$crud->change_field_type('status', 'enum', array('yes','no'));
		$crud->required_fields('username', 'nama', 'username', 'email','phone', 'alamat', 'password');
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
  		//$crud->callback_before_update(array($this,'encrypt_password_callback'));
		$crud->callback_edit_field('password',array($this,'decrypt_password_callback'));
		$data['grocery'] = $crud->render();
		$this->load->view('administrator/index', array_merge($data, $this->session_data) , FALSE);
	}

	function panelTeaser()
	{
		
		$data['title'] = 'Halaman Dashboard Administrator | Panel Teaser';
		$data['navbreadcrumb'] = 'Teaser';
		$data['maincontent'] = 'administrator/panel/panelTeaser';
		$this->grocery_crud->set_table('tbl_teaser');
		$data['grocery'] = $this->grocery_crud->render();
 		
 		//var_dump($output->js_files);
        $this->load->view('administrator/index', array_merge($data, $this->session_data));
	}

	function panelOrder()
	{

	}

	function panelRestaurant()
	{
		

		$data['title'] = 'Halaman Dashboard Administrator | Panel Restaurant';
		$data['navbreadcrumb'] = 'Panel Restaurant';
		$data['maincontent'] = 'administrator/panel/panelRestaurant';
		$crud = new grocery_CRUD();
		$crud->set_table('tbl_restaurant');
		$crud->unset_add_fields('id_restaurant');
		$crud->unset_edit_fields('id_restaurant');
		$crud->change_field_type('kategori', 'enum', array('cafe','minuman','jajanan','rmpadang'));
		$data['grocery'] = $crud->render();
 		
 		//var_dump($output->js_files);
        $this->load->view('administrator/index', array_merge($data, $this->session_data));
	}

	

	function panelMenu()
	{

		$data['title'] = 'Halaman Dashboard Administrator | Panel Menu';
		$data['navbreadcrumb'] = 'Panel Menu';
		$data['maincontent'] = 'administrator/panel/panelMenu';
		$crud = new grocery_CRUD();

		$crud->set_table('tbl_menu');
		$crud->columns('id_menu', 'nama_menu', 'harga', 'deskripsi','picture', 'id_restaurant');
		$crud->display_as('id_restaurant', 'Nama Restaurant')
			 ->display_as('nama_menu', 'Nama Menu');
		$crud->set_subject('Menu');
		$crud->unset_add_fields('id_menu');
		$crud->unset_edit_fields('id_menu');
		$crud->set_field_upload('picture', 'assets/uploads/menu/');
		$crud->set_relation('id_restaurant','tbl_restaurant','nama');
		$data['grocery'] = $crud->render();
 		
 		//var_dump($output->js_files);
        $this->load->view('administrator/index', array_merge($data));
	}


	function panelKurir()
	{
		$data['title'] = 'Halaman Dashboard Administrator | Panel Kurir';
		$data['navbreadcrumb'] = 'Panel Kurir';
		$data['maincontent'] = 'administrator/panel/panelKurir';
		$crud = new grocery_CRUD();
		$crud->set_table('tbl_kurir');
		$crud->set_subject('Kurir');
		$crud->unset_add_fields('id_kurir');
		$crud->unset_edit_fields('id_kurir');
		$data['grocery'] = $crud->render();
		$this->load->view('administrator/index', array_merge($data));
	}

	function create_email_content()
	{

	}

	function send_email_teaser($useremail)
	{
		$this->load->library('email');
		$this->load->helper('email');
		if (valid_email($useremail))
		{
			$this->email->from('no-reply@vrala.com', 'Validasi');
			$this->email->to($useremail);
			//$this->email->cc('another@example.com');
			//$this->email->bcc('and@another.com');
			$this->email->subject('Email Confirmation');
			$this->email->message($this->create_email_content());
			$this->email->send();
			echo $this->email->print_debugger();
            }
            
        }
	
	function adminLogout()
	{
		$this->session->unset_userdata('admin');
		$this->session->sess_destroy();
		redirect('home/login_admin', 'refresh');
	}


}
?>