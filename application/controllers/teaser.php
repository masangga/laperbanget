<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teaser extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('teaser_model');
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		
		
	}
	
	public function index()
	{
		$data['title'] = "Laperbanget.com | Online Food Order Delivery";
		$this->load->view('teaser/index', $data);
	}

	function subscribe()
	{
		$data['title'] = "Laperbanget.com | Online Food Order Delivery | Subsribe";
		$this->load->view('teaser/subscribe', $data);
	}

	function faq()
	{
		$data['title'] = "Laperbanget.com | Online Food Order Delivery | FAQ";
		$this->load->view('teaser/faq', $data);
	}

	function underconstruction()
	{
		$data['title'] = 'under construction';
		$this->load->view('underconstruction', $data);
	}

	function addSubscriber()
	{
		$this->load->model('admin_model');
		$email = getPost("email");
		$email_exist = $this->teaser_model->is_exist_email($email);
		if ($email_exist)
		{
			//kalau email udah ada
			echo 'exist';
		}
		else
		{
			//kalau email belum ada
			$data['email'] = $email;
			$data['time'] = date("Y-m-d H:i:s");
			$data['ip'] = $this->input->ip_address();
			if ($this->admin_model->insert_teaser($data))
			{
				echo 'adding';
			}
			else
			{
				echo 'cant add';
			}

		}
		
	}

	public function facebook_request()
	{
		$this->load->library('fbconnect');

		$data['redirect_uri'] = local_path('teaser/handle_facebook_login');
		$data['scope'] = 'email';
		redirect($this->fbconnect->getLoginUrl($data));
	}

	public function handle_facebook_login()
	{
		$this->load->library('fbconnect');
		$this->load->model('admin_model');
		$facebook_user = $this->fbconnect->user;
		if ($this->fbconnect->user)
		{
			if ($this->teaser_model->is_exist_email($facebook_user['email']))
			{
				// jika email sudah ada di teaser
				$this->session->set_flashdata('email', 'email is already exist!');
			}
			else
			{
				// jika email belum ada di database teaser
				// masukkan ke dalam database
				$data['email'] = $facebook_user['email'];
				$data['time'] = date("Y-m-d H:i:s");
				$data['ip'] = $this->input->ip_address();
				$data['facebook_id'] = $facebook_user['id'];
				$data['fb_name'] = $facebook_user['name'];
				$data['fb_link'] = $facebook_user['link'];
				if ($this->admin_model->insert_teaser($data))
				{
					$this->session->set_flashdata('email', 'terima kasih sudah mensubscribe laperbanget.com!');
				}
				else
				{
					$this->session->set_flashdata('email', 'error in adding email');
				}
				
			}
			redirect('teaser', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('email', 'error while subscribing');
			redirect('teaser', 'refresh');
		}
	}

	public function subscribe_with_twitter()
	{
		$this->load->library('twitter');
		$this->load->model('admin_model');
		if($this->twitter->is_logged_in())
		{
			//echo 'hello berhasil';
			$twitter_user = $this->twitter->call('get', 'account/verify_credentials');
			if ($this->teaser_model->is_exist_twitter_username($twitter_user['screen_name']))
			{
				//username udah ada
				$this->session->set_flashdata('twitter', 'username is already exist!');
			}
			else
			{
				//username belum ada
				//tambahkan ke dalam database
				$data['time'] = date("Y-m-d H:i:s");
				$data['ip'] = $this->input->ip_address();
				$data['twitter_name'] = $twitter_user["name"];
				$data['twitter_friends'] = $twitter_user["friends_count"];
				$data['twitter_username'] = $twitter_user["screen_name"];
				if ($this->admin_model->insert_teaser($data))
				{
					$this->session->set_flashdata('email', 'terima kasih sudah mensubscribe laperbanget.com!');
				}
				else
				{
					$this->session->set_flashdata('email', 'error in adding email');
				}
				redirect('teaser', 'refresh');
			}
            
		}
		else
		{
			//echo "maaf kurang beruntung";
			$this->twitter->login();
		}
	}

}

/* End of file teaser.php */
/* Location: ./application/controllers/teaser.php */
?>