<?php 

/**
* 
*/
class Utama extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$this->load->model('user_model');
	}

	function index()
	{
		$this->load->view('utama/index', $data, FALSE);
	}
}
?>