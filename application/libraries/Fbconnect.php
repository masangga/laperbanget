<?php

/**
* 
*/

include(APPPATH.'libraries/facebook/facebook.php');
class Fbconnect extends Facebook
{
	public $user = NULL;
	public $user_id = NULL;
	public $fb = false;
	public $fbSession = false;
	public $appkey = 0;

	public function Fbconnect()
	{
		$ci =& get_instance();
		$ci->config->load('facebook', TRUE);
		$config = $ci->config->item('facebook');
		parent::__construct($config);

		$this->user_id = $this->getUser();
		$me = NULL;
		if ($this->user_id)
		{
			try 
			{
				$me = $this->api('/me');
				$this->user = $me;
			}
			catch (FacebookApiException $e)
			{
				error_log($e);
			}
		}

	}
}