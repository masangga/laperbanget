<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	//function to generate url in local
	if ( ! function_exists('local_a_href')){
		function local_a_href($url,$text,$attrib=""){
			return anchor(base_url()."index.php/".$url,$text,$attrib);
		}
	}
	if ( ! function_exists('ext_a_href')){
		function ext_a_href($url,$text,$attrib=""){
			return "<a href='$url' $attrib>$text</a>";
		}
	}
	
	//function to generate link
	if ( ! function_exists('local_url')){
		function local_url($url){
			return base_url()."index.php/".$url;
		}
	}
	
	//function to generate link
	if ( ! function_exists('local_path')){
		function local_path($url){
			return base_url().$url;
		}
	}
	
	//function to generate timestamp
	if ( ! function_exists('getTimeStamp')){
		function getTimeStamp(){
			date_default_timezone_set ("Asia/Jakarta");
			return date("Y-m-d H:i:s");
		}
	}

	//function to generate timestamp
	if ( ! function_exists('getDateNow')){
		function getDateNow(){
			date_default_timezone_set ("Asia/Jakarta");
			return date("Y-m-d");
		}
	}

	
	if ( ! function_exists('readable_date')){
		function readable_date($tgl){
			$tgl = explode("-", $tgl);
			$bulan = array("","January","February","March","April","May","June","July","August","September","October","November","December");
			return $tgl[2]." ".$bulan[intval($tgl[1])]." ".$tgl[0];
		}
	}
	
	
	if ( ! function_exists('escape')){
		function escape($var,$post=true){
			$var=($post==true)?getPost($var):$var;
			if(is_array($var)){	
				foreach($var as $id=>$val){
					$var[$id]=htmlentities(trim(mysql_real_escape_string($val)));
				}
				return $var;
			}else{
				return htmlentities(trim(mysql_real_escape_string($var)));
			}
		}
	}
	
	if ( ! function_exists('getPost')){
		function getPost($var){
			if (!isset($_POST[$var])){
			    return "";
			}else{
			    return $_POST[$var];
			}
		}
	}
	
	if ( ! function_exists('filter_tag')){
		function filter_tag($var,$alw=""){
			$allowTag=($alw=="")?"<br>":$alw;
			if(is_array($var)){	
				foreach($var as $id=>$val){
					$var[$id]=str_replace("\n","<br />",$var[$id]);
					$var[$id]=str_replace("\r","",$var[$id]);
					$var[$id]=str_replace("\\r","",$var[$id]);
					$var[$id]=str_replace("\\n","<br />",$var[$id]);
					$var[$id]=strip_tags(trim(mysql_real_escape_string($val)),$allowTag);
				}
				return $var;
			}else{
				$var=str_replace("\n","<br />",$var);
				$var=str_replace("\r","",$var);
				$var=str_replace("\\r"," ",$var);
				$var=str_replace("\\n","<br />",$var);
				return strip_tags(trim(mysql_real_escape_string($var)),$allowTag);
			}
		}
	}
	
	
?>